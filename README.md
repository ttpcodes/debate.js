# debate.js
> A potential concept for prep software for use in Extemporaneous,
Parliamentary, and other speech and debate events.

debate.js is a free and open source alternative to numerous other debate softwares
in existence, such as Extemp Genie.

Unlike other services such as Extemp Genie, debate.js is built to be completely
free and independent of any servers or other infrastructure. To do this, debate.js
maintains its own local database, its own API to add plugins for parsing various
news websites, and its own parsing and pulling functionality.

## Usage
Download the latest release for your platform [here](https://github.com/TehTotalPwnage/debate.js/releases).

### Readers
Once you have the latest release installed, you will need to install a "Reader",
which is a plugin that adds support for a specific news website. For example, the
plugin [ArsTechnicaReader](https://github.com/TehTotalPwnage/debate.js-readers/tree/master/src/Ars%20Technica)
adds support for fetching articles from [Ars Technica](https://arstechnica.com).
The path for readers varies by platform:
- **Windows**: %APPDATA%/debate.js/readers
- **macOS**: ~/Library/Application Support/debate.js/readers
- **Linux**: ~/.config/debate.js/readers

Download the plugin or write your own using the API. The plugin should be a JavaScript
file, and should be placed in the reader directory. Once a reader has been installed,
the software must be restarted to reload all the readers.

### Downloading Articles
Three buttons are available in the home page for managing articles:
- Add Article gives you the option of providing a link to an article. The application
will attempt to find the appropriate reader and open up a menu to allow you to tag
information about the article before saving. If a reader isn't found, the application
will warn you that no reader was found supporting that website.
- View Articles gives you access to a table with all the articles. Basic searching
allows you to sort and search by article title and publication date. Clicking on
an article will load it in a viewer for inspection.
- Pull Articles allows you to select a news source to load front page headlines
from. The application will create a prompt for each article that is found and cycle
through them similar to the Add Article prompt until all of the articles are parsed.

### Other Functionality
The sidebar pullout gives you a choice between a light and dark theme. Both themes
are based off of the [Bootswatch](https://bootswatch.com/) collection of Bootstrap
themes.

At the top is a timer which will allow you to track your preparation time.

## API
You should not need to read this unless you are planning on contributing to debate.js's
codebase or if you would like to design a custom parser.

### Creating Readers
If you don't already know what a "Reader" is, read the section regarding Readers
above.

To implement a reader, you will need to export a class that extends the "BaseReader"
class built into the application which contains all of the HTTP request and DOM
parsing functionality needed to parse the article.

The export of your plugin should be a function with a BaseReader parameter so that
the application can call the function and provide the BaseReader for the plugin
to consume.

```javascript
module.exports = function (BaseReader) {
  class MyReader extends BaseReader {
    // Here we override all of the needed functions.
  }
  return MyReader
}
```

Instructions on which functions to override will be posted as the API is stabilized.

### Development
To set up the build environment for this application, clone this repository and
use `npm` to install the required dependencies. Below are the various commands built
into electron-vue that will help with development:

``` bash
# Install dependencies
npm install

# Serve application with hot reload at localhost:9080
npm run dev

# Build electron application for production
npm run build

# Run unit & end-to-end tests
npm test

# Lint all JS/Vue component files in `src/`
npm run lint

```

## Caveats
- Yes, I [know](https://www.techrepublic.com/article/series-of-critical-bugs-in-npm-are-destroying-server-configurations/)
that `npm` is a raging dumpster fire of a package manager and that even electron-vue
recommends switching to `yarn`. Yarn, however, [doesn't](https://github.com/yarnpkg/yarn/issues/756)
have a command for rebuilding dependencies, meaning that [this](https://github.com/mapbox/node-sqlite3#custom-builds-and-electron)
patch needed to run `sqlite3` on `electron` simply doesn't work. As a result, this
software will continue to use `npm` for dependency management until either a different
database is found (and justifiably replaces sqlite3) or yarn adds proper rebuilding
support.
