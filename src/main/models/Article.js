module.exports = function (sequelize, DataTypes) {
  return sequelize.define('article', {
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    published: {
      type: DataTypes.DATE
    },
    modified: {
      type: DataTypes.DATE
    },
    source: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    limit: 1000
  })
}
