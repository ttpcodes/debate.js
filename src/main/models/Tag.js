module.exports = function (sequelize, DataTypes) {
  return sequelize.define('tag', {
    name: {
      type: DataTypes.STRING,
      unique: true
    }
  }, {
    limit: 1000
  })
}
