/**
 * This export is meant to rename the require function and make it available for
 * dynamic modules. This allows require to be used in its original Node.js
 * context without Webpack's interference.
 */
module.exports = require
