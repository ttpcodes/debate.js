import Vue from 'vue'
import axios from 'axios'

import App from './App'

import Add from './components/article/edit/Add'
import DatePicker from 'vuejs-datepicker'
import ErrorModal from './components/modals/ErrorModal'
import Form from './components/article/Form'
import Navbar from './components/Navbar'
import Puller from './components/Puller'
import Search from './components/SearchCollapse'
import Success from './components/article/edit/Success'
import Timer from './components/Timer'

import router from './router'
import store from './store'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.component('add', Add)
Vue.component('datepicker', DatePicker)
Vue.component('error', ErrorModal)
Vue.component('editform', Form)
Vue.component('navbar', Navbar)
Vue.component('puller', Puller)
Vue.component('search', Search)
Vue.component('success', Success)
Vue.component('timer', Timer)

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
